<?php
/**
 * Created by PhpStorm.
 * User: rob
 * Date: 2-1-16
 * Time: 20:29
 */

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$parcel_weight =  "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    if(isset($_GET["weight"])){
        $parcel_weight = test_input($_GET["weight"]);
        echo "Parcel weight: ".$parcel_weight. " grams.";
    }
    if(isset($_GET["barcode"])){
        $parcel_barcode = test_input($_GET["barcode"]);
        echo "Parcel barcode: ".$parcel_barcode;
    }
}
?>