# Parcel scanner #

The parcel scanner will be used to check the weight of a parcel with a digital scale and a barcode scanner connected to a Raspberry Pi.

When the barcode label on a parcel is scanned, the scale is read and and this data is sent to a server with an HTTP GET request. The returned data from the server (barcode and weight are checked) will be displayed on an LCD screen and, depending on the returned data, a certain audio tone should be played through a speaker.

## File list ##

* parcel-scanner.py should run on the Raspberry Pi
* parcelscanner.php should be placed on the server