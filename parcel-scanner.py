import sys
import usb.core
import usb.util
import httplib

hid = { 4: 'a', 5: 'b', 6: 'c', 7: 'd', 8: 'e', 9: 'f', 10: 'g', 11: 'h', 12: 'i', 13: 'j', 14: 'k', 15: 'l', 16: 'm', 17: 'n', 18: 'o', 19: 'p', 20: 'q', 21: 'r', 22: 's', 23: 't', 24: 'u', 25: 'v', 26: 'w', 27: 'x', 28: 'y', 29: 'z', 30: '1', 31: '2', 32: '3', 33: '4', 34: '5', 35: '6', 36: '7', 37: '8', 38: '9', 39: '0', 44: ' ', 45: '-', 46: '=', 47: '[', 48: ']', 49: '\\', 51: ';' , 52: '\'', 53: '~', 54: ',', 55: '.', 56: '/' }
hid2 = { 4: 'A', 5: 'B', 6: 'C', 7: 'D', 8: 'E', 9: 'F', 10: 'G', 11: 'H', 12: 'I', 13: 'J', 14: 'K', 15: 'L', 16: 'M', 17: 'N', 18: 'O', 19: 'P', 20: 'Q', 21: 'R', 22: 'S', 23: 'T', 24: 'U', 25: 'V', 26: 'W', 27: 'X', 28: 'Y', 29: 'Z', 30: '!', 31: '@', 32: '#', 33: '$', 34: '%', 35: '^', 36: '&', 37: '*', 38: '(', 39: ')', 44: ' ', 45: '_', 46: '+', 47: '{', 48: '}', 49: '|', 51: ':' , 52: '"', 53: '~', 54: '<', 55: '>', 56: '?' }

fp = open('/dev/hidraw0', 'rb')

barcode_data = ""
shift = False
done = False

while not done:
    ## Get the character from the HID
    buffer = fp.read(8)
    
    for c in buffer:
        if ord(c) > 0:
            ## 40 is carriage return which signifies
            ## we are done looking for characters
            if int(ord(c)) == 40:
                done = True
                break;
            ## If we are shifted then we have to
            ## use the hid2 characters. if shift:
            ## If it is a '2' then it is the shift key
            if int(ord(c)) == 2 :
                shift = True

            ## if not a 2 then lookup the mapping
            else:
                barcode_data += hid2[ int(ord(c)) ]
                shift = False

        ## If we are not shifted then use
        ## the hid characters

        else:

            ## If it is a '2' then it is the shift key
            if int(ord(c)) == 2 :
                shift = True

            ## if not a 2 then lookup the mapping
            else:
                barcode_data += hid[ int(ord(c)) ]

print barcode_data

weight = None

def read_scale():
    # set USB Dymo scale vendor and product id
    SCALE_VENDOR_ID = 0x0922
    SCALE_PRODUCT_ID = 0x8009

    # find the USB Dymo scale
    dymo_scale = usb.core.find(idVendor=SCALE_VENDOR_ID, idProduct=SCALE_PRODUCT_ID)

    if dymo_scale is None:
        print("Could not find the Dymo Scale.")
        exit
    
    if dymo_scale.is_kernel_driver_active(0):
        try:
            dymo_scale.detach_kernel_driver(0)
        except usb.core.USBError as e:
            sys.exit("Could not detatch kernel driver: %s" % str(e))

    # use the first/default configuration
    dymo_scale.set_configuration()
    # first endpoint
    endpoint = dymo_scale[0][(0,0)][0]

    # read a data packet
    attempts = 10
    data = None
    while data is None and attempts > 0:
        try:
            data = dymo_scale.read(endpoint.bEndpointAddress, endpoint.wMaxPacketSize)
        except usb.core.USBError as e:
            data = None
            if e.args == ('Operation timed out',):
                attempts -= 1
                continue

    # get the weight in grams
    weight = (data[4] + (256 * data[5]))*100
    return weight;

if barcode_data != None:
    weight = read_scale();

if weight != None:
    # connect to the server
    conn = httplib.HTTPSConnection("www.ditverzinjeniet.nl")
    # send the weight data to our script on the server
    conn.request("GET", "/parcelscanner.php?weight="+str(weight))
    r1 = conn.getresponse()
    #print r1.status
    if r1.status == 200:
        print r1.read()
    else:
        print "Error connecting to server."
